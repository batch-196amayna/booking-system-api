/*
	The naming convention for model files is the singular and capitalized form of the name of the documents
*/
const mongoose = require("mongoose");
/*
	Mongoose Schema
	before we can create documents from our api to save into our database, we must first determine the structure of the documents to be written in the database

	Schema acts as a blueprint for our data/document

	A schema is a representation of how the document is structured. It also determines the types of data and the expected properties.

	So, with this, we won't have to worry for if we has input "stock" or "stocks" in our documents, because we can make it uniform with a schema

	In mongoose, to create a schema, we use the Schema() constructor from mongoose. This will allow us to create a new mongooes schema object
*/

const courseSchema = new mongoose.Schema({

	name:{
		type:String,
		required:[true, "Name is required"]
	},
	description:{
		type: String,
		required:[true, "Description is required"]
	},
	price:{
		type: Number,
		required:[true, "Price is required"]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	createdOn:{
		type: Date,
		default: new Date()
	},
	enrollees:[
		{
			userId:{
				type:String,
				required:[true, "User ID is required"]
			},
			dateEnrolled:{
				type:Date,
				default: new Date()
			},
			status:{
				type: String,
				default:"Enrolled"
			}
		}
	]
})
//module.exports - so we can import and use thos file in another file.
/*
	Mongoose Model
	is the connection to our collection
	it has two arguments, first the name of the collection the model is going to connect to. In mongoDB, once we created a new course document this model will connect to your courses collection. But since the courses collection is not yet created initially, mongoDB will create it for us

	second is the schema of the documents in the collection
*/

module.exports = mongoose.model("Course", courseSchema);