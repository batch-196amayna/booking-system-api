const express = require("express");
const mongoose = require("mongoose");
//Mongoose is an ODM library to let our ExpressJS API manipulated a MongiDB database

const app = express();
const port = 4000;

/*
	Mongoose Connection
	mongoose.connect() is a method to connect our api with our mongodb database via the use of mongoose.
	it has 2 arguments. first, is the connection string to connect our api to our mongodb
	second, is an object used to add information between mongooes and mongodb
*/

mongoose.connect("mongodb+srv://admin:admin123@cluster0.0pkkjbd.mongodb.net/bookingAPI?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});
//should be in string

let db = mongoose.connection;
db.on('error', console.error.bind(console, "MongoDB Connection Error."));

db.once('open',()=>console.log("Connected to MongoDB"));
app.use(express.json())

//import our routes and use it as middleware
//which means, that we will be able to group together our routes
const courseRoutes = require('./routes/courseRoutes');

//use our routes and group the
app.use('/courses',courseRoutes);


const userRoutes = require('./routes/userRoutes');

app.use('/users',userRoutes);

app.listen(port,() => console.log(`Express API running at post 4000`));


