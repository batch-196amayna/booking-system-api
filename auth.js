/*
	auth.js is our own module which will contain methods to help authorize or restrict users from accessing certain features in our application
*/

const jwt = require("jsonwebtoken");

//This is the secret string which validated or which will use to check the validity of a passed token. If a token does not contain this secret string, then that token is invalid or illegitimate
const secret = "courseBookingAPI"
/*
	JWT is a way to secretly pass information from one part of a server to the frontend or other parts of our application. this will allow us to authorize our users to access or disallow access certains of our application

*/
module.exports.createAccessToken = (userDetails)=>{
	//pick only certain details fro  our user to be included in the token.
	//password should not be included
	//console.log(userDetails);

	const data = {
		id: userDetails.id,
		email: userDetails.email,
		isAdmin: userDetails.isAdmin
	}
	console.log(data);

	//jwt.sign() will create a JWT using our data object, with our secret
	//{} default algorithim 
	return jwt.sign(data,secret,{});

}

module.exports.verify = (req,res,next)=>{
let token = req.headers.authorization

	if(typeof token === "undefined"){
		return res.send({auth:"Failed. No Token"})
	} else {
		/*
			When passing JWT we use the Bearer Token authorization. This means that when JWT is passed a word "Bearer" as well as space is added.

			slice() and copy the rest of the token without the word Bearer

			slice(<stringPosition>,<endPosition>)

			Update thee token variable with the sliced version
		*/
		console.log(token);
		token = token.slice(7);
		//console.log(token);

		//3 aruments token, the secret and a handler function which will handle either an error if token is invalid or the decoded data from the token
		jwt.verify(token,secret,function(err,decodedToken){

			//console.log(decodedToken); 
			//contains the data if the token is verified
			//console.log(err); 
			//null as there is no error

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			} else {
				req.user = decodedToken;
				next();
			}
		})
	}
}

	//verifyAdmin wil be used as a middleware
	//It has to follow or be added after verify(), so that we can check for the validity and add the decodedToken is the request object as req.user
	module.exports.verifyAdmin = (req,res,next)=>{
		console.log(req.user);
		//check if user is admin
		if(req.user.isAdmin){
			//if the user is admin, proceed to the next middleware or controller
			next();
		} else {
			return res.send({
				auth:"Failed",
				message:"Action Forbidden"
			})
		}
	}