/*
	To be able to create routes from another file to be used in our application, from here, we have to import express as well, however, we will now use another method from express to contain our routes

	the Router() method, will let us contain our routes
*/

const express = require("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers")
//import the courseControllers inside the courseRoutes

const auth = require("../auth");
const {verify,verifyAdmin} = auth;

//all routes to courses now has an endpoint prefaced with /courses
//endpoint - /courses/
//get all courses
router.get('/',verify,verifyAdmin,courseControllers.getAllCourses);

//endpoint - /courses/
//only logged in user ia able to use addCourse
//verifyAdmin will only work once the logged is passed to verify
router.post('/',verify,verifyAdmin,courseControllers.addCourse);

router.get('/activeCourses', courseControllers.getActiveCourses)


//pass data in a route without the use of request body by passing a small amount of data through the url with the use of route params
//http://localhost:4000/courses/getSingleCourse/62e7506d971540be53e3538c
//whatever name we put will indicate in the route param
router.get("/getSingleCourse/:courseId", courseControllers.getSingleCourse);



//update a single course
//pass the id of the course we want to update via route params
//the update details will be passed via request body
router.put("/updateCourse/:courseId",verify,verifyAdmin,courseControllers.updateCourse);


//archive/delete a single course
//pass the id for the course we want to update via route params
//we will directly update the course as inactive
router.delete("/archiveCourse/:courseId",verify,verifyAdmin,courseControllers.archiveCourse);

module.exports = router;