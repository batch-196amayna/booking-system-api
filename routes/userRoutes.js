const express = require("express");
const router = express.Router();

//In fact, routes should just contain the endpoints and it should only trigger the function but it should not be where we define the logic of our function
//The business logic of our API should be in controllers

const userControllers = require("../controllers/userControllers");

console.log(userControllers);

const auth = require("../auth");

//destructure auth to get only our methods ans save it on variable.
const {verify} = auth;
/*
	Update Route Syntax:
	method("/endpoint", handlerFunction)
*/

//register
//userRouter has been connected userControllers that contains codes
router.post("/", userControllers.registerUser);

//verify() is a middleware that the request will get through (lik a gate)
//verify() will not only check the validity of the token but also add the decoded data of the token in the request object as req.user
router.get("/details", verify, userControllers.getUserDetails);


//route for user authentication
router.post('/login', userControllers.loginUser);


router.post("/checkEmail", userControllers.checkEmail);

//User Eenrollment
//courseId will come from the req.body
//userId will come from the req.user
router.post('/enroll',verify,userControllers.enroll);

module.exports = router;