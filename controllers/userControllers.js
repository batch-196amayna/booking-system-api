/*
	Naming convention for controllers is that it should be named after the model/ documents it is connected with.

	Controllers are functions which contain the actual business logic of our API and is triggered by a route.

	MVC - models, views and controller
*/

//To create a controller, we first add it into our module.export
//so that we can import the controllers from our module

//import the User model in the controllers instead because this is where we aree now going to use it
const User = require("../models/User")

//import course
const Course = require("../models/Course")

//import bcrypt
//bcryt is a package which allows us to hash our passwords to add a layer of security for our user's details
const bcrypt = require("bcrypt");

//import auth.js module to use createAccessToken and its subsequest methods
const auth = require("../auth");



//userController.registerUser
module.exports.registerUser = (req,res)=>{

	const hashedPw = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPw)

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo
	});

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.getUserDetails = (req,res)=>{
	//console.log(req.user)//contains the details of the logged in user
	//console.log(req.body);

	//User.find(req.body)

	//find() will return an array of documents which matches the criteria
	//it will return an array even if it only found 1 document
	//User.find({_id:req.body.id})

	//findOne() will return a single document that matched our criteria
	//User.findOne({_id:req.body.id})
	
	//findId() is a mongoose method that will allow us to find a document strictly by its id
	//User.findById(req.body.id)-for specific id

	//This will allow us to ensure that the LOGGED IN USER or the USER THAT PASSED THE TOKEN will be able to get HIS OWN details and ONLY his own
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}



module.exports.loginUser = (req,res)=>{
	console.log(req.body);
	/*
		steps for logging in our users:

		1. find the user by its email
		2. if we found the user, we will check his password if the password input and the hashed password in our db matches
		3. if we don't find a user, we will send a message to the client
		4. if upon checking the found user's password is the same our input password, we will generate a "Key" a token for our user, else, we will send a message
	*/

	User.findOne({email:req.body.email})
	.then(foundUser => {

		//foundUser is the parameter that contains the result of findOne
		if(foundUser === null){
			return res.send({message:"No User Found"})
		//client will receive the message if not found
		} else {
			//console.log(foundUser)
		//if found, the foundUser will contain the document that matched email

		//check if password from req.body matches the hashed password in our foundUser document
		/*
			bcrypt.compareSync(<inputString>,<hashedString>)

			"sample1234"
			"$2b$10$ATVKJyON69maI.s3zxjs7.3JmewqNHWM9hUmxRBnWpi2mOHBBr4pG"

			if the inputString and hashedString matched, the cpmpareSync method will return true, else it will be false

		*/
		const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
		//console.log(isPasswordCorrect); to check isPasswordCorrect

		//if the password is correct, we will create a "Key", a token for our user, else, we will send a message

		//isPasswordCorrect can use as it is boolean (true or false)
		if(isPasswordCorrect){
			/*
				To be able to create a "key" or token that allows/authorizes our logged in user around our application, we have to create our own module called auth.js

				This module will create a encoded string which
			*/
			//console.log("We will create a token for the user if the password is correct")
			
			//auth.createAcessToken receives our foundUser documents
			return res.send({accessToken: auth.createAccessToken(foundUser)});

		} else {
			return res.send({message:"Incorrect Password"});
		}



		}
	})



	
	.catch(error => res.send(error))
}

//if the email is no match false, else true
module.exports.checkEmail = (req,res)=>{
	User.findOne({email:req.body.email})
	.then(result => {
		if(result === null){
			return res.send(false)
		} else {
			return res.send(true)
		}
	})	
	.catch(error => res.send(error))
}

module.exports.enroll = async (req,res)=>{

	// console.log(req.user.id);
	// console.log(req.body.courseId);

	//validate if the user is an admin
	if(req.user.isAdmin){
		return res.send({message:"Action Forbidden."});
	}
	/*
		2 steps of Enrollement:
		first, find the user who is enrolling and update his enrollments subdocument array and push the courseId in the enrollment array
		second, find the course where we are enrolling and update its enrollees subdocument and push the userId in the enrollees array

		since we will access 2 collectio s in one action, we will have to wait for the completion of the action instead of letting Javascript continue line per line

		async and await - async keyword is addded to a function to make our function asynchronous. which means that instead of JS regular behavior of running each code line by line we will be able to wait for the result function

		to be able to wait for the result of a function we use the awit keyword. the await keyword allows us to wait for the function
	*/

	//return a boolean to our isUserUpdated variable to determine the result of the query and dif we were able to save the courseId into our user's enrollment
	let isUserUpdated = await User.findById(req.user.id).then(user =>{
		//console.log(user); //check user's document in the terminal

		//add the courseId in a object and push that object into the user's enrolment 
		//we have to follow the schema of the enrollemts subdocument array

		let newEnrollment = {
			courseId: req.body.courseId
		}

		//access the enrollments array from our user and push the new enrollment subdicument into the enrollments array
		user.enrollments.push(newEnrollment)
		
		//we must save the user document and return the value of saving our document
		//then return true IF we push the subdocument successfully
		//catch and return error otherwise
		return user.save().then(user=>true).catch(err=>err.message)
	})

	//if the user was able to enroll, isUserUpdated is true
	//else isUserUpdated is error
	//console.log(isUserUpdated);

	if(isUserUpdated !== true){
		return res.send({message:isUserUpdated});
	}

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		console.log(course); //contains the found courses or the course we want to enroll in

		//create an object to pushed into the subdocument array, enrollees

		let enrollee = {
			userId: req.user.id
		}
		//push the enrolles into the enrollees subdocument array of the course
		course.enrollees.push(enrollee);
		//save the course document
		//return true if we were able to save and add the user as enrollee
		//return err otherwise
		return course.save().then(course=>true).catch(err=>err.message);
	})
	//console.log(isCourseUpdated);
	if(isCourseUpdated!== true){
		return res.send({message: isCourseUpdated})
	}
	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Thank you for enrolling"})
	}
}