//import the Course model so we can manipulate it and add a new course document
const Course = require("../models/Course");

module.exports.getAllCourses = (req,res)=>{
	//use the Course model to connect to our collectin and retrieve our courses
	//to be able to query into our collections we use the model connected to that collection
	//in mongoDB: db.courses.find({})
	//Model.find() - returns a collection of documents that matches our criteria similar to mongodb's .find()
	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.addCourse = (req,res)=>{
	//console.log(req.body); //to check if we receive

	//using the Course model, we will use its constructir to create our Course document which will follow the schema of the model, and add methods fro documents creation
	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	});

	//console.log(newCourse); to check in terminal

	//newCourse is now an object which follows the courseSchema and with additional methods from our Course constructor

	//.save() method is added into our newCourse object. This will allow us to save the content of newCourse into our collection
	//db.courses.inserOne()

	//.then() allows us to process the result of a previous function/method in its own anonymous function

	//.catch() catches the errors and aloows us to process and process and send to the client. without catch, the server will crash
	newCourse.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.getActiveCourses = (req,res)=>{

	Course.find({isActive:true})
	.then(result=>res.send(result))
	.catch(error=>res.send(error))
}

module.exports.getSingleCourse = (req,res)=>{
	//console.log(req.params)
	//req.params is an object contains the value captured via route params
	//the field name req.params indicate the name of the route params

	console.log(req.params.courseId)

	Course.findById(req.params.courseId)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.updateCourse = (req,res)=>{
	console.log(req.params.courseId)//to check the id
	console.log(req.body)//to check the update that is input

	//findByIdAndUpdate - used to update documents and has 3 arguments
	//findByIdAndUpdate(<id>,{update},{new:true})
	//fields/property that are not part of the update object will not be updated
	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.archiveCourse = (req,res)=>{

	console.log(req.params.courseId)

	let update = {
		isActive: false
	}

	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

